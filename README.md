# DESCRIPTION #

- Ручное добавление каналов;
- Предустановленные RSS каналы при каждом запуске добавляются, если отсутствуют;
- Автоматическое обновление списка новостей при переходе на канал;
- Ручное обновление списка новостей с помощью Refresh Control;
- Парсер пытается угадать формат часового пояса;
- Кеширование полученных новостей в базе данных для оффлайн просмотра;
- Работа с Core Data в фоновых потоках;
- RSS новости добавляются кумулятивно при обновлении;
- Динамичная высота ячеек;
- Протестировано iOS 8.1-11.1;
- Поддержка iOS 7 (не протестировано по техническим причинам);
- ReactiveCocoa 2.5, TyphoonFramework.
- Xcode 9.1

"Warning: Automatic Preferred Max Layout Width before iOS 8.0" учтен в коде. Смотреть ODARssItemTableViewCell.m, -(void)layoutSubviews

# VIDEO #

[![Watch the video](https://bytebucket.org/Awaking/rssreader/raw/22888781d45049b0cdfdc06f148f24b9d3b93d70/ReadmeMedia/RssReader_video_preview.png)](https://youtu.be/mBp5d35WFGI)

### SCREENSHOTS ###
![screenshot_1](https://bytebucket.org/Awaking/rssreader/raw/22888781d45049b0cdfdc06f148f24b9d3b93d70/ReadmeMedia/RssReader_screenshot_1.png)
![screenshot_2](https://bytebucket.org/Awaking/rssreader/raw/22888781d45049b0cdfdc06f148f24b9d3b93d70/ReadmeMedia/RssReader_screenshot_2.png)
