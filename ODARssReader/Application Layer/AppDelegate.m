//
//  AppDelegate.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "ODAApplicationAssembly.h"
#import "ODACoreDataStack.h"
#import "ODARssCaching.h"
#import <ReactiveCocoa/ReactiveCocoa.h>


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"Populating with predefined channels...");
    [[[self.assembly rssCacher] populateContextWithPredefinedRssChannels] subscribeCompleted:^{
        [self.coreDataStack saveMainContext];
    }];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self.coreDataStack saveMainContext];
}


@end
