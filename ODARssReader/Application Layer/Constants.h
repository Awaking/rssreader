//
//  Constants.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSArray<NSDictionary<NSString *, NSString *> *> * TransientRssItem;

FOUNDATION_EXPORT NSString * _Nonnull const ODATransientRssItemKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODATransientRssTitleKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODATransientRssLinkKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODATransientRssDescriptionKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODATransientRssPubDateKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODATransientRssGuidKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODARssDateZormatWithoutZone;

FOUNDATION_EXPORT NSString * _Nonnull const ODAMainStoryboardName;

FOUNDATION_EXPORT NSString * _Nonnull const ODACoreDataRssChannelEntityName;
FOUNDATION_EXPORT NSString * _Nonnull const ODACoreDataRssItemEntityName;

FOUNDATION_EXPORT NSString * _Nonnull const ODAPredefinedRssChannelNameKey;
FOUNDATION_EXPORT NSString * _Nonnull const ODAPredefinedRssChannelLinkKey;


//Title or descriprion of rss used as primary key by trimming to this value if origin guid is empty
FOUNDATION_EXPORT NSUInteger const ODAMaxSurrogateGuidLenght;

//Mute NSLog() in release
#ifdef DEBUG
#   define NSLog(...) NSLog(__VA_ARGS__)
#else
#   define NSLog(...) (void)0
#endif

@interface ODAConstants : NSObject

//Dictionary with ODAPredefinedRssChannelNameKey:NSString and ODAPredefinedRssChannelLinkKey:NSString
+(NSArray<NSDictionary<NSString *, NSString *> *> * _Nullable) predefinedRssSources;

//All supported datetime formats for NSDateFormatter
+(NSArray<NSString*> * _Nonnull) supportedDateFormats;
@end
