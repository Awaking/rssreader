//
//  Constants.c
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#include "Constants.h"

NSString * const ODATransientRssItemKey = @"item";
NSString * const ODATransientRssTitleKey = @"title";
NSString * const ODATransientRssLinkKey = @"link";
NSString * const ODATransientRssDescriptionKey = @"description";
NSString * const ODATransientRssPubDateKey = @"pubDate";
NSString * const ODATransientRssGuidKey = @"guid";
NSString * const ODARssDateZormatWithoutZone = @"EEE, dd MMM yyyy HH:mm:ss";

NSString * const ODAMainStoryboardName = @"Main";

NSString * const ODACoreDataRssChannelEntityName = @"RssChannel";
NSString * const ODACoreDataRssItemEntityName = @"RssItem";


NSString * const ODAPredefinedRssChannelNameKey = @"ODAPredefinedRssChannelNameKey";
NSString * const ODAPredefinedRssChannelLinkKey = @"ODAPredefinedRssChannelUrlKey";

NSUInteger const ODAMaxSurrogateGuidLenght = 35;

@implementation ODAConstants

+(NSArray<NSDictionary<NSString *, NSString *> *> * _Nullable) predefinedRssSources {
    return @[@{ODAPredefinedRssChannelNameKey : @"Lenta.Ru",     ODAPredefinedRssChannelLinkKey : @"https://lenta.ru/rss"},
             @{ODAPredefinedRssChannelNameKey : @"Apple Developer News",     ODAPredefinedRssChannelLinkKey : @"http://developer.apple.com/news/rss/news.rss"},
             @{ODAPredefinedRssChannelNameKey : @"Meduza", ODAPredefinedRssChannelLinkKey : @"https://meduza.io/rss/podcasts/meduza-v-kurse"},
             @{ODAPredefinedRssChannelNameKey : @"NYT - World", ODAPredefinedRssChannelLinkKey : @"https://rss.nytimes.com/services/xml/rss/nyt/World.xml"},
             @{ODAPredefinedRssChannelNameKey : @"NASA Education News", ODAPredefinedRssChannelLinkKey : @"https://www.nasa.gov/rss/dyn/educationnews.rss"}];
}

+(NSArray<NSString*> * _Nonnull) supportedDateFormats {
    static dispatch_once_t onceToken;
    static NSArray *formats;
    
    dispatch_once(&onceToken, ^{
        NSString *baseFormat = [ODARssDateZormatWithoutZone stringByAppendingString:@" "];
        NSMutableArray *allFormats = @[baseFormat].mutableCopy;
        
        //Association between character and it's max count
        NSArray *zoneVariations = @[@{@"z" : @4},
                                    @{@"Z" : @5},
                                    @{@"O" : @4},
                                    @{@"v" : @4},
                                    @{@"V" : @4},
                                    @{@"X" : @5},
                                    @{@"x" : @4},
                                    ];
        
        for(NSDictionary<NSString *, NSNumber *> *pair in zoneVariations) {
            NSString *character = pair.allKeys.firstObject;
            NSNumber *maxCount = pair.allValues.firstObject;
            for (int i=0; i<maxCount.intValue; i++) {
                [allFormats addObject:[baseFormat stringByPaddingToLength:baseFormat.length + i + 1
                                                               withString:character
                                                          startingAtIndex:0]];
            }
        }
        formats = allFormats.copy;
    });

    return formats;
}
@end
