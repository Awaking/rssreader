//
//  AppDelegate.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ODACoreDataStack;
@class ODACoreDataStack, ODAApplicationAssembly;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic, nonnull) UIWindow *window;
@property (strong, nonatomic, nonnull) id<ODACoreDataStack> coreDataStack;
@property (strong, nonatomic, nonnull) ODAApplicationAssembly *assembly;

@end

