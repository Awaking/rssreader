//
//  UINavigationItem+ODAActivityIndicatorItem.m
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "UINavigationItem+ODAActivityIndicatorItem.h"

@implementation UINavigationItem (ODAActivityIndicatorItem)

-(UIActivityIndicatorView *) addRightIndicatorView {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    activityIndicator.color = [UIColor blackColor];
    activityIndicator.hidesWhenStopped = YES;
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.rightBarButtonItem = barButton;
    return activityIndicator;
}

@end
