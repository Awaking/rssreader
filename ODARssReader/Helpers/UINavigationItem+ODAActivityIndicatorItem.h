//
//  UINavigationItem+ODAActivityIndicatorItem.h
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (ODAActivityIndicatorItem)

/**
 Add UIActivityIndicatorView as a right bar button item and return

 @return Added UIActivityIndicatorView
 */
-(UIActivityIndicatorView *) addRightIndicatorView;

@end
