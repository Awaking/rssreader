//
//  NSString+ODAStringHelpers.h
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ODAStringHelpers)

-(NSString *) trimmedNewLineWhitespaceString;

@end
