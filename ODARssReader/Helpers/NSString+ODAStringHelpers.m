//
//  NSString+ODAStringHelpers.m
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "NSString+ODAStringHelpers.h"

@implementation NSString (ODAStringHelpers)

-(NSString *) trimmedNewLineWhitespaceString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
