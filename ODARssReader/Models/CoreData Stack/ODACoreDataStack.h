//
//  ODACoreDataStack.h
//  ODARssReader
//
//  Created by Denis Obukhov on 14/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol ODACoreDataStack <NSObject>

@property (readonly, strong, nonatomic) NSManagedObjectContext * _Nonnull managedObjectContext;

/**
 Sync. Save main managed object context
 */
- (void)saveMainContext;


/**
 Used to perform background tasks
 @return Managed object context which is child for the main managed object context
 */
- (NSManagedObjectContext * _Nonnull)backgroundManagedObjectContext;

@end
