//
//  ODACoreDataStackImpl.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ODACoreDataStack.h"


@interface ODACoreDataStackImpl : NSObject <ODACoreDataStack>

@property (readonly, strong, nonatomic) NSManagedObjectContext * _Nonnull managedObjectContext;

@end
