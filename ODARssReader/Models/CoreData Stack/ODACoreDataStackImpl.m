//
//  ODACoreDataStackImpl.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODACoreDataStackImpl.h"
#import "Constants.h"
#import "RssChannel.h"

@interface ODACoreDataStackImpl()
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

@implementation ODACoreDataStackImpl


- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ODARssReader" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ODARssReader.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
    [options setObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
    [options setObject:[NSNumber numberWithBool:YES] forKey:NSInferMappingModelAutomaticallyOption];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"ODARssReaderErrorDomain" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *) managedObjectContext {
    static NSManagedObjectContext *managedObjectContext = nil;
    static dispatch_once_t onceToken;
    
    /// https://sophiebits.com/2014/04/02/dispatch-once-initialization-on-the-main-thread.html
    if ([NSThread isMainThread]) {
        dispatch_once(&onceToken, ^{
            managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            managedObjectContext.undoManager = nil;
            managedObjectContext.parentContext = self.privateManagedObjectContext;
        });
    } else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            dispatch_once(&onceToken, ^{
                managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
                managedObjectContext.parentContext = self.privateManagedObjectContext;
                managedObjectContext.undoManager = nil;
            });
        });
    }
    return managedObjectContext;
}

- (NSManagedObjectContext *)privateManagedObjectContext {
    static NSManagedObjectContext *privateManagedObjectContext;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        privateManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        privateManagedObjectContext.undoManager = nil;
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (!coordinator) {
            NSLog(@"Error: No coordinator for privateManagedObjectContext");
            abort();
        }
        [privateManagedObjectContext setPersistentStoreCoordinator:coordinator];
    });
    return privateManagedObjectContext;
}

- (NSManagedObjectContext * _Nonnull)backgroundManagedObjectContext {
    NSManagedObjectContext *backgroundContext = [[NSManagedObjectContext alloc]initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    backgroundContext.parentContext = [self managedObjectContext];
    return backgroundContext;
}


#pragma mark - Core Data Saving support


- (void)saveMainContext {
    [self.managedObjectContext performBlock:^{
        NSError *error = nil;        
        //Bug fix
        //Read:
        //https://stackoverflow.com/questions/11990279/core-data-do-child-contexts-ever-get-permanent-objectids-for-newly-inserted-obj
        [self.managedObjectContext obtainPermanentIDsForObjects:self.managedObjectContext.insertedObjects.allObjects error:&error];
        if (error)
            NSLog(@"Error obtaining permanent IDs: %@", error.localizedDescription);
        
        if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
            /// TODO: delete abort()
        }

        [self.privateManagedObjectContext performBlock:^{
            NSError *error = nil;
            if ([self.privateManagedObjectContext hasChanges] && ![self.privateManagedObjectContext save:&error]) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
                // TODO: delete abort()
            }

        }];
    }];
}

#pragma mark - Misc

// The directory the application uses to store the Core Data store file.
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
