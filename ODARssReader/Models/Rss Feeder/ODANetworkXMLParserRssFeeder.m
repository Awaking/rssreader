//
//  ODARssFeeder.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODANetworkXMLParserRssFeeder.h"
#import "Constants.h"
#import "RssChannel.h"
#import "RssItem.h"
#import <ReactiveCocoa/ReactiveCocoa.h>


@interface ODANetworkXMLParserRssFeeder() <NSXMLParserDelegate>
@end

@implementation ODANetworkXMLParserRssFeeder

-(void)dealloc {
    NSLog(@"ODARssFeeder - dealloc");
}


- (RACSignal * _Nonnull) fetchTransientRssItemsForUrl: (NSURL * _Nonnull) sourceUrl {
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        __block NSMutableArray<NSDictionary *> *rssItemsInfo = @[].mutableCopy;
        __block NSString *currentElement;
        __block NSMutableDictionary *currentItem;
        __block NSMutableString *currentElementTitle;
        __block NSMutableString *currentElementLink;
        __block NSMutableString *currentElementDescription;
        __block NSMutableString *currentElementPubDate;
        __block NSMutableString *currentElementGuid;

        RACSignal *didStartElementSignal = [self rac_signalForSelector:@selector(parser:didStartElement:namespaceURI:qualifiedName:attributes:)
                                                          fromProtocol:@protocol(NSXMLParserDelegate)];
        [didStartElementSignal subscribeNext:^(RACTuple *args) {
            currentElement = args.second;
            if ([currentElement isEqualToString:ODATransientRssItemKey]) {
                currentItem = [[NSMutableDictionary alloc] init];
                currentElementTitle = [[NSMutableString alloc] init];
                currentElementLink = [[NSMutableString alloc] init];
                currentElementDescription = [[NSMutableString alloc] init];
                currentElementPubDate = [[NSMutableString alloc] init];
                currentElementGuid = [[NSMutableString alloc] init];
            }
        }];

        RACSignal *foundCharactersSignal = [self rac_signalForSelector:@selector(parser:foundCharacters:)
                                                          fromProtocol:@protocol(NSXMLParserDelegate)];
        [foundCharactersSignal subscribeNext:^(RACTuple *args) {
            NSString *charactersToAddend = args.second;
            if ([currentElement isEqualToString:ODATransientRssTitleKey]) {
                [currentElementTitle appendString:charactersToAddend];
            } else if ([currentElement isEqualToString:ODATransientRssLinkKey]) {
                [currentElementLink appendString:charactersToAddend];
            } else if ([currentElement isEqualToString:ODATransientRssDescriptionKey]) {
                [currentElementDescription appendString:charactersToAddend];
            } else if ([currentElement isEqualToString:ODATransientRssPubDateKey]) {
                [currentElementPubDate appendString:charactersToAddend];
            } else if ([currentElement isEqualToString:ODATransientRssGuidKey]) {
                [currentElementGuid appendString:charactersToAddend];
            }
        }];

        RACSignal *didEndElementSignal = [self rac_signalForSelector:@selector(parser:didEndElement:namespaceURI:qualifiedName:)
                                                        fromProtocol:@protocol(NSXMLParserDelegate)];
        [didEndElementSignal subscribeNext:^(RACTuple *args) {
            NSString *elementName = args.second;
            if ([elementName isEqualToString:ODATransientRssItemKey]) {
                currentItem[ODATransientRssTitleKey] = currentElementTitle;
                currentItem[ODATransientRssLinkKey] = currentElementLink;
                currentItem[ODATransientRssDescriptionKey] = currentElementDescription;
                currentItem[ODATransientRssPubDateKey] = currentElementPubDate;
                currentItem[ODATransientRssGuidKey] = currentElementGuid;
                [rssItemsInfo addObject:currentItem.copy];
            }
        }];

        RACSignal *parsingDidEndSignal = [self rac_signalForSelector:@selector(parserDidEndDocument:)
                                                        fromProtocol:@protocol(NSXMLParserDelegate)];

        [parsingDidEndSignal subscribeNext:^(id x) {
            [subscriber sendNext:rssItemsInfo];
            [subscriber sendCompleted];
        }];

        RACSignal *parseErrorSignal = [self rac_signalForSelector:@selector(parser:parseErrorOccurred:)
                                                fromProtocol:@protocol(NSXMLParserDelegate)];
        
        
        RACSignal *validationErrorSignal = [self rac_signalForSelector:@selector(parser:validationErrorOccurred:)
                                                fromProtocol:@protocol(NSXMLParserDelegate)];

        [[RACSignal merge:@[parseErrorSignal, validationErrorSignal]] subscribeNext:^(RACTuple *args) {
            @strongify(self)
            [subscriber sendError:[self handleError:args.second]];
        }];

        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration: config];
        NSURLSessionDataTask *downloadTask = [session
                                              dataTaskWithURL:sourceUrl
                                              completionHandler:^(NSData *xmlData, NSURLResponse *response, NSError *error) {
                                                  if (error) {
                                                      [subscriber sendError:[self handleError:error]];
                                                      return;
                                                  }
                                                  //Additional converion using text encoding from NSURLResponse.
                                                  //Otherwise will get periodically errors NSXMLParserErrorDomain
                                                  //Code=5 "Extra content at the end of the document"
                                                  NSString *encodingName = [response textEncodingName];
                                                  NSStringEncoding encodingType = CFStringConvertEncodingToNSStringEncoding(CFStringConvertIANACharSetNameToEncoding((CFStringRef)encodingName));
                                                  NSString *xmlString = [[NSString alloc] initWithData:xmlData
                                                                                              encoding:encodingType];
                                                  NSData *data = [xmlString dataUsingEncoding:encodingType];
                                                  NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
                                                  [parser setDelegate:self];
                                                  if(![parser parse]) {
                                                      [subscriber sendError:[self handleError:parser.parserError]];
                                                  }
                                              }];
        [downloadTask resume];
        
        return [RACDisposable disposableWithBlock:^{
            [downloadTask cancel];
        }];
    }];
}


-(NSError *) handleError: (NSError *) errorToHandle {
    //Replace faceless localized description with pretty text for NSXMLParserErrorDomain errors
    if ([errorToHandle.domain isEqualToString:NSXMLParserErrorDomain] && errorToHandle.userInfo[@"NSXMLParserErrorMessage"]) {
        NSMutableDictionary *userInfo = errorToHandle.userInfo.mutableCopy;
        NSString *newLocalizedDescription = [NSString stringWithFormat:@"%@ code: %ld, domain: %@",
                                             NSLocalizedString(errorToHandle.userInfo[@"NSXMLParserErrorMessage"], @""),
                                             (long)errorToHandle.code,
                                             errorToHandle.domain];
        userInfo[NSLocalizedDescriptionKey] = newLocalizedDescription;
        return [[NSError alloc] initWithDomain:errorToHandle.domain code:errorToHandle.code userInfo:userInfo];
    } else if ([errorToHandle.domain isEqualToString:NSURLErrorDomain] && errorToHandle.code == NSURLErrorNotConnectedToInternet) {
        //Don't show the error if there is no internet connecton. Get rid of annoying error messages for comfort offline browsing
        return nil;
    }
    return errorToHandle;
}

@end
