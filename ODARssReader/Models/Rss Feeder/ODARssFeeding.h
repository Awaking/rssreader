//
//  ODARssFeeding.h
//  ODARssReader
//
//  Created by Denis Obukhov on 14/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@class RACSignal;

@protocol ODARssFeeding <NSObject>

/**
 Obtain RSS feed items

 @param sourceUrl Url for the RSS Feed
 @return RACSignal that emits TransientRssItem with keys: ODATransientRssTitleKey, ODATransientRssLinkKey, ODATransientRssDescriptionKey, ODATransientRssPubDateKey
 */
- (RACSignal * _Nonnull) fetchTransientRssItemsForUrl: (NSURL * _Nonnull) sourceUrl;

@end
