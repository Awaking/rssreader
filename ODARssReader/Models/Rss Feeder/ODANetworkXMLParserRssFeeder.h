//
//  ODARssFeeder.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ODARssFeeding.h"


@interface ODANetworkXMLParserRssFeeder : NSObject <ODARssFeeding>

@end

