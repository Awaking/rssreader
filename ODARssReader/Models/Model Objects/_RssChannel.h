// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RssChannel.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class RssItem;

@interface RssChannelID : NSManagedObjectID {}
@end

@interface _RssChannel : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RssChannelID *objectID;

@property (nonatomic, strong) NSString* feedUrl;

@property (nonatomic, strong) NSString* name;

@property (nonatomic, strong, nullable) NSSet<RssItem*> *rssItems;
- (nullable NSMutableSet<RssItem*>*)rssItemsSet;

@end

@interface _RssChannel (RssItemsCoreDataGeneratedAccessors)
- (void)addRssItems:(NSSet<RssItem*>*)value_;
- (void)removeRssItems:(NSSet<RssItem*>*)value_;
- (void)addRssItemsObject:(RssItem*)value_;
- (void)removeRssItemsObject:(RssItem*)value_;

@end

@interface _RssChannel (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveFeedUrl;
- (void)setPrimitiveFeedUrl:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet<RssItem*>*)primitiveRssItems;
- (void)setPrimitiveRssItems:(NSMutableSet<RssItem*>*)value;

@end

@interface RssChannelAttributes: NSObject 
+ (NSString *)feedUrl;
+ (NSString *)name;
@end

@interface RssChannelRelationships: NSObject
+ (NSString *)rssItems;
@end

NS_ASSUME_NONNULL_END
