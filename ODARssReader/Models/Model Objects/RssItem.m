#import "RssItem.h"

@interface RssItem ()

// Private interface goes here.

@end

@implementation RssItem

#pragma mark - Transient properties


-(NSDate *)dateForSection {
    [self willAccessValueForKey:@"dateForSection"];
    NSDate *tmp = [self primitiveValueForKey:@"dateForSection"];
    [self didAccessValueForKey:@"dateForSection"];
    
    if (!tmp) {
        tmp = [self simplifiedDateForDate: [self publicationDate]];
        [self setPrimitiveValue:tmp forKey:@"dateForSection"];
    }
    
    return tmp;
}

-(NSDate *)simplifiedDateForDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:(NSMonthCalendarUnit | NSYearCalendarUnit)
                                                              fromDate:date];
    NSDate *newDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    return newDate;
}

+(NSSet *)keyPathsForValuesAffectingSectionIdentifier {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"publicationDate"];
}

@end
