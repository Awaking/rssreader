// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RssItem.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class RssChannel;

@interface RssItemID : NSManagedObjectID {}
@end

@interface _RssItem : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RssItemID *objectID;

@property (nonatomic, strong, nullable) NSDate* dateForSection;

@property (nonatomic, strong, nullable) NSString* fullDescription;

@property (nonatomic, strong) NSString* guid;

@property (nonatomic, strong, nullable) NSString* link;

@property (nonatomic, strong, nullable) NSDate* publicationDate;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong) RssChannel *rssChannel;

@end

@interface _RssItem (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSDate*)primitiveDateForSection;
- (void)setPrimitiveDateForSection:(nullable NSDate*)value;

- (nullable NSString*)primitiveFullDescription;
- (void)setPrimitiveFullDescription:(nullable NSString*)value;

- (NSString*)primitiveGuid;
- (void)setPrimitiveGuid:(NSString*)value;

- (nullable NSString*)primitiveLink;
- (void)setPrimitiveLink:(nullable NSString*)value;

- (nullable NSDate*)primitivePublicationDate;
- (void)setPrimitivePublicationDate:(nullable NSDate*)value;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (RssChannel*)primitiveRssChannel;
- (void)setPrimitiveRssChannel:(RssChannel*)value;

@end

@interface RssItemAttributes: NSObject 
+ (NSString *)dateForSection;
+ (NSString *)fullDescription;
+ (NSString *)guid;
+ (NSString *)link;
+ (NSString *)publicationDate;
+ (NSString *)title;
@end

@interface RssItemRelationships: NSObject
+ (NSString *)rssChannel;
@end

NS_ASSUME_NONNULL_END
