// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RssChannel.m instead.

#import "_RssChannel.h"

@implementation RssChannelID
@end

@implementation _RssChannel

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"RssChannel" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"RssChannel";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"RssChannel" inManagedObjectContext:moc_];
}

- (RssChannelID*)objectID {
	return (RssChannelID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic feedUrl;

@dynamic name;

@dynamic rssItems;

- (NSMutableSet<RssItem*>*)rssItemsSet {
	[self willAccessValueForKey:@"rssItems"];

	NSMutableSet<RssItem*> *result = (NSMutableSet<RssItem*>*)[self mutableSetValueForKey:@"rssItems"];

	[self didAccessValueForKey:@"rssItems"];
	return result;
}

@end

@implementation RssChannelAttributes 
+ (NSString *)feedUrl {
	return @"feedUrl";
}
+ (NSString *)name {
	return @"name";
}
@end

@implementation RssChannelRelationships 
+ (NSString *)rssItems {
	return @"rssItems";
}
@end

