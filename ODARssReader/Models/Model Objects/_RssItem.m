// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RssItem.m instead.

#import "_RssItem.h"

@implementation RssItemID
@end

@implementation _RssItem

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"RssItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"RssItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"RssItem" inManagedObjectContext:moc_];
}

- (RssItemID*)objectID {
	return (RssItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic dateForSection;

@dynamic fullDescription;

@dynamic guid;

@dynamic link;

@dynamic publicationDate;

@dynamic title;

@dynamic rssChannel;

@end

@implementation RssItemAttributes 
+ (NSString *)dateForSection {
	return @"dateForSection";
}
+ (NSString *)fullDescription {
	return @"fullDescription";
}
+ (NSString *)guid {
	return @"guid";
}
+ (NSString *)link {
	return @"link";
}
+ (NSString *)publicationDate {
	return @"publicationDate";
}
+ (NSString *)title {
	return @"title";
}
@end

@implementation RssItemRelationships 
+ (NSString *)rssChannel {
	return @"rssChannel";
}
@end

