//
//  ODARssCacher.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODACoreDataRssCacher.h"
#import "Constants.h"
#import "NSString+ODAStringHelpers.h"
#import "ODACoreDataStack.h"
#import "RssChannel.h"
#import "RssItem.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ODACoreDataRssCacher ()
@property (strong, nonnull, nonatomic) id<ODACoreDataStack> coreDataStack;
@end

@implementation ODACoreDataRssCacher

- (instancetype _Nonnull)initWithCoreDataStack: (id<ODACoreDataStack> _Nonnull) coreDataStack {
    self = [super init];
    if (self) {
        _coreDataStack = coreDataStack;
    }
    return self;
}

-(void)dealloc {
    NSLog(@"ODARssCacher - dealloc");
}

-(RACSignal * _Nonnull) updateRssItemInfo:(NSArray<NSDictionary<NSString *, NSString *> *> * _Nonnull) rssItemsInfo forRssChannel:(RssChannel * _Nonnull) rssChannel {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSManagedObjectID *rssChannelObjectID = rssChannel.objectID;
        NSManagedObjectContext *backgroundContext = [self.coreDataStack backgroundManagedObjectContext];
        [backgroundContext performBlock:^{
            //Obtaining rssChannel MO in background context
            NSError *error;
            RssChannel *backgroundRssChannel = [backgroundContext existingObjectWithID:rssChannelObjectID error:&error];
            if (error) {
                NSLog(@"Error fetching RssChannel by ID: %@, %@", rssChannelObjectID, error);
                [subscriber sendError:error];
                return;
            }
            
            for (NSDictionary *rssItemInfo in rssItemsInfo) {
                NSString *title = [rssItemInfo[ODATransientRssTitleKey] trimmedNewLineWhitespaceString];
                NSString *link = [rssItemInfo[ODATransientRssLinkKey] trimmedNewLineWhitespaceString];
                NSString *fullDescription = [rssItemInfo[ODATransientRssDescriptionKey] trimmedNewLineWhitespaceString];
                NSString *pubDate = [rssItemInfo[ODATransientRssPubDateKey] trimmedNewLineWhitespaceString];
                NSString *guid = [rssItemInfo[ODATransientRssGuidKey] trimmedNewLineWhitespaceString];
                
                //Try to substitute guid with another data is it's empty
                if (!guid.length) {
                    if (link.length) {
                        guid = link;
                    } else if (title.length) {
                        guid = [title substringToIndex:MIN(ODAMaxSurrogateGuidLenght, title.length)];
                    } else if (fullDescription.length) {
                        guid = [fullDescription substringToIndex:MIN(ODAMaxSurrogateGuidLenght, fullDescription.length)];
                    } else {
                        continue;
                    }
                }

                //Check, is there any rss item with the same link
                RssItem *currentRssItem = nil;
                for (RssItem *item in backgroundRssChannel.rssItems) {
                    if ([item.guid isEqualToString:guid]) {
                        currentRssItem = item;
                        break;
                    }
                }
                
                if (!currentRssItem) {
                    currentRssItem = [NSEntityDescription insertNewObjectForEntityForName:ODACoreDataRssItemEntityName
                                                                   inManagedObjectContext:backgroundContext];
                    currentRssItem.rssChannel = backgroundRssChannel;
                }
                currentRssItem.title = title;
                currentRssItem.link = link;
                currentRssItem.fullDescription = fullDescription;
                currentRssItem.guid = guid;
                
                NSDate *date;
                
                //Try to find out date format
                if (pubDate.length) {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    for (NSString *dateFormat in [ODAConstants supportedDateFormats]) {
                        [dateFormatter setDateFormat:dateFormat];
                        date = [dateFormatter dateFromString:pubDate];
                        if (date) {
                            break;
                        }
                    }
                    if (!date) {
                        //Try to parse without timezone
                        NSMutableArray *components = [pubDate componentsSeparatedByString:@" "].mutableCopy;
                        [components removeLastObject];
                        NSString *stringWithoutZone = [components componentsJoinedByString:@" "];
                        [dateFormatter setDateFormat:ODARssDateZormatWithoutZone];
                        date = [dateFormatter dateFromString:stringWithoutZone];
                    }
                }
                if (!date) {
                    date = [NSDate dateWithTimeIntervalSinceNow:0];
                }
                currentRssItem.publicationDate = date;
            }

            error = nil;
            if ([backgroundContext hasChanges] && ![backgroundContext save:&error]) {
                NSLog(@"Unresolved error while saving: %@", error);
                [subscriber sendError:error];
            }
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

-(RACSignal * _Nonnull) populateContextWithPredefinedRssChannels {
    return [ODAConstants.predefinedRssSources.rac_sequence.signal flattenMap:^RACStream *(NSDictionary *rssSource) {
        NSURL *url = [NSURL URLWithString:rssSource[ODAPredefinedRssChannelLinkKey]];
        return [self insertRssChannelNamed:rssSource[ODAPredefinedRssChannelNameKey] url:url];
    }];
}

-(RACSignal * _Nonnull) insertRssChannelNamed:(NSString * _Nonnull) name url:(NSURL *_Nonnull) url {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSManagedObjectContext *backgroundContext = [self.coreDataStack backgroundManagedObjectContext];
        [backgroundContext performBlock:^{
            NSString *stringURL = url.absoluteString;
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:ODACoreDataRssChannelEntityName];
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"feedUrl == %@", stringURL];
            fetchRequest.fetchLimit = 1;
            NSError *error;
            NSArray *results = [backgroundContext executeFetchRequest:fetchRequest
                                                                error:&error];
            if (error) {
                NSLog(@"Error fetching rss source: %@", [error localizedDescription]);
                [subscriber sendError:error];
                return;
            }
            RssChannel *existingChannel = [results firstObject];
            
            //Create new channel only if there is no existing channel with the same url
            if (!existingChannel) {
                existingChannel = [NSEntityDescription insertNewObjectForEntityForName:ODACoreDataRssChannelEntityName
                                                                inManagedObjectContext:backgroundContext];
                existingChannel.feedUrl = stringURL;
            }
            existingChannel.name = name;
            error = nil;
            if ([backgroundContext hasChanges] && ![backgroundContext save:&error]) {
                NSLog(@"Unresolved error while saving context: %@", error);
                [subscriber sendError:error];
                return;
            }
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

-(RACSignal * _Nonnull) deleteRssChannel: (RssChannel * _Nonnull) rssChannel {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSManagedObjectContext *backgroundContext = [self.coreDataStack backgroundManagedObjectContext];
        NSManagedObjectID *rssChannelObjectID = rssChannel.objectID;
        [backgroundContext performBlock:^{
            //Obtaining rssChannel MO in background context
            NSError *error;
            RssChannel *backgroundRssChannel = [backgroundContext existingObjectWithID:rssChannelObjectID error:&error];
            if (error) {
                NSLog(@"Error fetching RssChannel by ID: %@, %@", rssChannelObjectID, error);
                [subscriber sendError:error];
                return;
            }
            [backgroundContext deleteObject:backgroundRssChannel];
            error = nil;
            if ([backgroundContext hasChanges] && ![backgroundContext save:&error]) {
                NSLog(@"Unresolved error while saving context: %@", error);
                [subscriber sendError:error];
                return;
            }
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}
     
@end
