//
//  ODARssCaching.h
//  ODARssReader
//
//  Created by Denis Obukhov on 14/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "Constants.h"
#import <Foundation/Foundation.h>

@class RssChannel, RACSignal;


@protocol ODARssCaching <NSObject>

/**
 Async. Create or update existing RSS items info for specified channel

 @param rssItemsInfo TransientRssItem with keys: ODATransientRssTitleKey, ODATransientRssLinkKey, ODATransientRssDescriptionKey, ODATransientRssPubDateKey
 @param rssChannel Channel where RSS items will be added
 @return RACSignal which completes when operation was successfull else emits NSError.
 */
-(RACSignal * _Nonnull) updateRssItemInfo:(TransientRssItem _Nonnull) rssItemsInfo forRssChannel:(RssChannel * _Nonnull) rssChannel;


/**
 Async. Populate storage with predefined rss channels from [ODAConstants predefinedRssSources]

 @return RACSignal which completes when operation was successfull else emits NSError.
 */
-(RACSignal * _Nonnull) populateContextWithPredefinedRssChannels;


/**
 Async. Insert new RSS channel or update name of existing with the same url

 @param name Name of the channel
 @param url Url of the channel
 @return RACSignal which completes when operation was successfull else emits NSError.
 */
-(RACSignal * _Nonnull) insertRssChannelNamed:(NSString * _Nonnull) name url:(NSURL *_Nonnull) url;


/**
 Async. Delete specified channel

 @param rssChannel Channel to delete
 @return RACSignal which completes when operation was successfull else emits NSError.
 */
-(RACSignal * _Nonnull) deleteRssChannel: (RssChannel * _Nonnull) rssChannel;

@end
