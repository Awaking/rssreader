//
//  ODARssCacher.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODARssCaching.h"
#import <Foundation/Foundation.h>

@protocol ODACoreDataStack;
@class NSManagedObjectContext;

@interface ODACoreDataRssCacher : NSObject <ODARssCaching>

-(instancetype _Nonnull) init NS_UNAVAILABLE;
-(instancetype _Nonnull) initWithCoreDataStack: (id<ODACoreDataStack> _Nonnull) coreDataStack
NS_DESIGNATED_INITIALIZER;

@end
