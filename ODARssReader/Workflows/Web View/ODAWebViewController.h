//
//  ODAWebViewController.h
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ODAWebViewModel;

@interface ODAWebViewController : UIViewController

@property (strong, nonatomic, nonnull) ODAWebViewModel *viewModel;

@end
