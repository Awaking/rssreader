//
//  ODAWebViewModel.h
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RssItem, RACCommand, RACSignal;

@interface ODAWebViewModel : NSObject

/// Injected by View
@property (strong, nonatomic, nullable) RACCommand *refreshWebViewCommand;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)initWithRssItem: (RssItem * _Nonnull) rssItem
NS_DESIGNATED_INITIALIZER;

/**
 Starts web content refreshing
 */
- (void) refreshContent;


/**
 Called by view to figure out which error should be shown

 @param error Error to check
 @return YES if error should be shown, else NO
 */
-(BOOL) shouldShowError: (NSError * _Nonnull) error;
@end
