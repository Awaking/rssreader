//
//  ODAWebViewController.m
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODAWebViewController.h"
#import "ODAWebViewModel.h"
#import "UINavigationItem+ODAActivityIndicatorItem.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ODAWebViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic, nullable) UIActivityIndicatorView *activityIndicator;
@end

@implementation ODAWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    self.activityIndicator = [self.navigationItem addRightIndicatorView];
    [self bindViewModel];
    [self.activityIndicator startAnimating];
    [self.viewModel refreshContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#ifdef DEBUG
-(void)dealloc {
    NSLog(@"ODAWebViewController - dealloc");
}
#endif

- (void) bindViewModel {
    @weakify(self)
    self.viewModel.refreshWebViewCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(NSURLRequest *request) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            @strongify(self)
            [self.webView stopLoading];
            [self.webView loadRequest:request];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
}


#pragma mark - UIWebViewDelegate


-(void)webViewDidFinishLoad: (UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self.activityIndicator startAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    //Ignoring cancelled error produced by view controller deallocation with running network requests
    if (![self.viewModel shouldShowError:error])
        return;
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:error.localizedDescription
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
    [self.activityIndicator stopAnimating];
}
@end
