//
//  ODAWebViewModel.m
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODAWebViewModel.h"
#import "RssItem.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ODAWebViewModel()
@property (strong, nonatomic, nonnull) RssItem *rssItem;
@end

@implementation ODAWebViewModel

- (instancetype _Nonnull)initWithRssItem: (RssItem * _Nonnull) rssItem {
    self = [super init];
    if (self) {
        _rssItem = rssItem;
    }
    return self;
}

#ifdef DEBUG
-(void)dealloc {
    NSLog(@"ODAWebViewModel - dealloc");
}
#endif

- (void) refreshContent {
    NSURLRequest *loginPageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.rssItem.link]
                                                      cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                  timeoutInterval:30];
    [self.refreshWebViewCommand execute:loginPageRequest];
}

-(BOOL) shouldShowError: (NSError * _Nonnull) error {
    if (error.code == NSURLErrorCancelled)
        return NO;
    return YES;
}

@end
