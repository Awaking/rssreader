//
//  ODARssItemsTableViewController.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ODARssItemsTableViewModel, ODAApplicationAssembly;

@interface ODARssItemsTableViewController : UITableViewController

@property (strong, nonatomic, nullable) ODARssItemsTableViewModel *viewModel;
@property (strong, nonatomic, nullable) ODAApplicationAssembly *assembly;

@end
