//
//  ODARssItemsTableViewController.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODARssItemsTableViewController.h"
#import "Constants.h"
#import "ODAApplicationAssembly.h"
#import "ODARssItemTableViewCell.h"
#import "ODARssItemsTableViewModel.h"
#import "ODAWebViewController.h"
#import "UINavigationItem+ODAActivityIndicatorItem.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ODARssItemsTableViewController ()
@property (strong, nonatomic, nullable) UIActivityIndicatorView *activityIndicator;
@property (assign, nonatomic) NSInteger *activityIndicatorTasks;
@end

@implementation ODARssItemsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicator = [self.navigationItem addRightIndicatorView];
    [self bindViewModel];
    [self setupRefreshControl];
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
}

-(void) setupRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithWhite:0.835 alpha:1.000];
    self.refreshControl.tintColor = [UIColor blackColor];
    @weakify(self)
    self.refreshControl.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        return [self updateContentsWithActivityIndicator];
    }];
}

-(void) bindViewModel {
    @weakify(self)
    [self.viewModel.contentUpdateSignal
         subscribeNext:^(RACTuple *argsFromBuffer) {
             @strongify(self);
             [self startActivityIndicator];
             //iOS 8 bug
             //See https://stackoverflow.com/a/33304216/4493731
             if (NSFoundationVersionNumber < NSFoundationVersionNumber_iOS_9_0) {
                 [self.tableView reloadData];
                 [self.tableView setNeedsLayout];
                 [self.tableView layoutIfNeeded];
             }
             [self.tableView reloadData];
             [self stopActivityIndicator];
     }];
    
    RAC(self, navigationItem.title) = RACObserve(self.viewModel, title);
    
    [[self updateContentsWithActivityIndicator]
     subscribeNext:^(id x) {
    }];

}

- (RACSignal *) updateContentsWithActivityIndicator {
    @weakify(self)
    return [[[[[self.viewModel updateContents] takeUntil:self.rac_willDeallocSignal]
              doCompleted:^{
                  @strongify(self)
                  [self stopActivityIndicator];
              }]
              doNext:^(id x) {
                  @strongify(self)
                  [self startActivityIndicator];
              }]
              doError:^(NSError *error) {
                  @strongify(self)
                  [self stopActivityIndicator];
                  if (error)
                      [self showErrorAlertWithText: error.localizedDescription];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    NSLog(@"ODARssItemsTableViewController - dealloc");
}

-(void) startActivityIndicator {
    self.activityIndicatorTasks++;
    if (!self.activityIndicator.isAnimating)
        [self.activityIndicator startAnimating];
}

-(void) stopActivityIndicator {
    self.activityIndicatorTasks--;
    if (self.activityIndicatorTasks <= 0) {
        self.activityIndicatorTasks = 0;
        if (self.activityIndicator.isAnimating)
            [self.activityIndicator stopAnimating];
    }
}

-(void) showErrorAlertWithText: (NSString *) text {
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:text
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}


#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ODAWebViewController *webViewController = [self.assembly webViewControllerForViewModel:[self.viewModel viewModelForIndexPath:indexPath]];
    if (webViewController)
        [self.navigationController pushViewController:webViewController animated:YES];
}


#pragma mark - UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.viewModel numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfItemsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ODARssItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(ODARssItemTableViewCell.class)
                                                                    forIndexPath:indexPath];
    cell.titleLabel.text = [self.viewModel cellTitleAtIndexPath:indexPath];
    cell.descriptionLabel.text = [self.viewModel cellSubTitleAtIndexPath:indexPath];
    cell.dateLabel.text =  [self.viewModel cellDateAtIndexPath:indexPath];

    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.viewModel titleForSection:section];
}

-(NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.viewModel sectionIndexTitles];
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.viewModel sectionForSectionIndexTitle:title atIndex:index];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
        return UITableViewAutomaticDimension;
    }
    
    //Manual calculating row height
    static dispatch_once_t onceToken;
    static ODARssItemTableViewCell *sizingCell;
    dispatch_once(&onceToken, ^{
        //One instance in memory to calculate dimensions
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass(ODARssItemTableViewCell.class)];
    });
    sizingCell.titleLabel.text = [self.viewModel cellTitleAtIndexPath:indexPath];
    sizingCell.descriptionLabel.text = [self.viewModel cellSubTitleAtIndexPath:indexPath];
    sizingCell.dateLabel.text =  [self.viewModel cellDateAtIndexPath:indexPath];
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGFloat height = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;

    return height;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
        return 300;
    }
    return UITableViewAutomaticDimension;
}

@end
