//
//  ODARssItemTableViewCell.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODARssItemTableViewCell.h"

@implementation ODARssItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews
{
    [super layoutSubviews];

    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_9_0) {
        // Make sure the contentView does a layout pass here so that its subviews have their frames set, which we
        // need to use to set the preferredMaxLayoutWidth below.
        [self.contentView setNeedsLayout];
        [self.contentView layoutIfNeeded];    

        // Set the preferredMaxLayoutWidth of the mutli-line bodyLabel based on the evaluated width of the label's frame,
        // as this will allow the text to wrap correctly, and as a result allow the label to take on the correct height.
        self.titleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleLabel.frame);
        self.descriptionLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.descriptionLabel.frame);
    }
}
@end
