//
//  ODARssItemsTableViewModel.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ODACoreDataStack;
@class RssChannel, RACSignal, ODAApplicationAssembly, ODAWebViewModel;

@interface ODARssItemsTableViewModel : NSObject

@property (strong, nonatomic, nullable, readonly) NSString *title;
@property (strong, nonatomic, nonnull, readonly) RACSignal *contentUpdateSignal;

- (instancetype _Nonnull) init NS_UNAVAILABLE;
- (instancetype _Nonnull)initWithCoreDataStack: (id<ODACoreDataStack> _Nonnull) coreDataStack
                                    rssChannel: (RssChannel * _Nonnull) rssChannel
                                      assembly: (ODAApplicationAssembly * _Nonnull) assembly
NS_DESIGNATED_INITIALIZER;

-(ODAWebViewModel * _Nullable) viewModelForIndexPath: (NSIndexPath * _Nonnull) indexPath;
-(NSInteger)numberOfSections;
-(NSInteger)numberOfItemsInSection:(NSInteger)section;
-(NSString * _Nullable) titleForSection:(NSInteger)section;
-(NSString * _Nullable) cellTitleAtIndexPath:(NSIndexPath * _Nonnull)indexPath;
-(NSString * _Nullable) cellSubTitleAtIndexPath:(NSIndexPath * _Nonnull)indexPath;
-(NSString * _Nullable) cellDateAtIndexPath:(NSIndexPath * _Nonnull)indexPath;
-(NSArray<NSString *> * _Nonnull) sectionIndexTitles;
-(NSInteger) sectionForSectionIndexTitle:(NSString * _Nonnull)title atIndex:(NSInteger)index;
-(RACSignal * _Nonnull) updateContents;
@end
