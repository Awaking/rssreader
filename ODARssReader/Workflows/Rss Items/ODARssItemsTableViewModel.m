//
//  ODARssItemsTableViewModel.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODARssItemsTableViewModel.h"
#import "Constants.h"
#import "ODAApplicationAssembly.h"
#import "ODACoreDataStack.h"
#import "ODARssCaching.h"
#import "ODARssFeeding.h"
#import "ODAWebViewModel.h"
#import "RssChannel.h"
#import "RssItem.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ODARssItemsTableViewModel() <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic, nonnull) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic, nonnull) id<ODACoreDataStack>coreDataStack;
@property (strong, nonatomic, nonnull) RssChannel *rssChannel;
@property (strong, nonatomic, nullable) id<ODARssFeeding> rssFeeder;
@property (strong, nonatomic, nonnull) ODAApplicationAssembly *assembly;
@property (strong, nonatomic, nullable) NSArray<NSString *> *indexTitles;
@end

@implementation ODARssItemsTableViewModel

- (instancetype _Nonnull)initWithCoreDataStack: (id<ODACoreDataStack> _Nonnull) coreDataStack
                                    rssChannel: (RssChannel * _Nonnull) rssChannel
                                      assembly: (ODAApplicationAssembly * _Nonnull) assembly{
    self = [super init];
    if (self) {
        _coreDataStack = coreDataStack;
        _rssChannel = rssChannel;
        _assembly = assembly;
        _title = rssChannel.name;
        _contentUpdateSignal = [[[self rac_signalForSelector:@selector(controllerDidChangeContent:)
                                                fromProtocol:@protocol(NSFetchedResultsControllerDelegate)]
                                     map:^id(id value) {
                                         return nil;
                                     }] deliverOnMainThread];
    }
    return self;
}

-(ODAWebViewModel * _Nullable) viewModelForIndexPath: (NSIndexPath * _Nonnull) indexPath {
    RssItem *rssItem = [self rssItemAtIndexPath:indexPath];
    if ([rssItem.link length])
        return [self.assembly webViewModelForRssItem:rssItem];
    return nil;
}

-(void)dealloc {
    NSLog(@"ODARssItemsTableViewModel - dealloc");
}

-(RACSignal * _Nonnull) updateContents {
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [subscriber sendNext:nil]; //To indicate that the process started
        //Check if previous task not completed
        if (self.rssFeeder) {
            [subscriber sendCompleted];
            return nil;
        }        
        self.rssFeeder = [self.assembly rssFeeder];
        id <ODARssCaching> rssCacher = [self.assembly rssCacher];
        NSURL *feedURL = [NSURL URLWithString:self.rssChannel.feedUrl];
        RACDisposable *disposable = [[[[self.rssFeeder fetchTransientRssItemsForUrl:feedURL]
                     subscribeOn:[RACScheduler scheduler]]
                     flattenMap:^RACStream *(NSArray *rssItemsInfo) {
                         return [rssCacher updateRssItemInfo:rssItemsInfo forRssChannel:self.rssChannel];
                     }]
                     subscribeNext:^(NSArray *rssItemsInfo) {
                        // NSLog(@"Items: %@", rssItemsInfo);
                     } error:^(NSError *error) {
                         NSLog(@"Error while parsing: %@", error);
                         [subscriber sendError:error];
                     } completed:^{
                         NSLog(@"Parsing Completed");
                         [subscriber sendCompleted];
                         [self.coreDataStack saveMainContext];
         }];
        return [RACDisposable disposableWithBlock:^{
            [disposable dispose];
            self.rssFeeder = nil;
        }];
    }] deliverOnMainThread];
}


#pragma mark - Table View


-(NSInteger)numberOfSections {
    NSInteger sectionsNumber = [[self.fetchedResultsController sections] count];
    return sectionsNumber;
}

-(NSInteger)numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return sectionInfo.numberOfObjects;
}

-(NSString * _Nullable)titleForSection:(NSInteger)section {
    //Objects grouped into sections by month and year
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    RssItem *rssItem = sectionInfo.objects.firstObject; //any object in section
    return [self stringFromDate:rssItem.publicationDate format:@"MMMM, yyyy"];
}

-(NSString * _Nullable) cellTitleAtIndexPath:(NSIndexPath * _Nonnull)indexPath {
    RssItem *rssItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return rssItem.title;
}

-(NSString * _Nullable) cellSubTitleAtIndexPath:(NSIndexPath * _Nonnull)indexPath {
    RssItem *rssItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return rssItem.fullDescription;
}

-(NSString * _Nullable) cellDateAtIndexPath:(NSIndexPath * _Nonnull)indexPath {
    RssItem *rssItem = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return [self stringFromDate:rssItem.publicationDate format:@"dd-MM-yyyy HH:mm"];
}

-(NSArray<NSString *> * _Nonnull) sectionIndexTitles {
    NSMutableArray<NSString *> *indexTitles = [NSMutableArray arrayWithCapacity:[self.fetchedResultsController sections].count];
    for (int i = 0; i < [self.fetchedResultsController sections].count; i++) {
        id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][i];
        RssItem *rssItem = sectionInfo.objects.firstObject; //any object in section
        [indexTitles addObject:[self stringFromDate:rssItem.publicationDate format:@"MM-yy"]];
    }
    self.indexTitles = indexTitles;
    return self.indexTitles;
}

-(NSInteger) sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.indexTitles indexOfObject:title];
}


#pragma mark - Misc


-(NSString *) stringFromDate:(NSDate *) date format: (NSString *) format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}

-(RssItem *) rssItemAtIndexPath: (NSIndexPath *) indexPath {
    return  [self.fetchedResultsController objectAtIndexPath:indexPath];
}


#pragma mark - Fetched results controller


- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchedResultsController *aFetchedResultsController;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setFetchBatchSize:20];
    fetchRequest.entity = [NSEntityDescription entityForName: ODACoreDataRssItemEntityName
                                      inManagedObjectContext:self.coreDataStack.managedObjectContext];
    NSSortDescriptor *internalSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"publicationDate" ascending:NO];
    NSSortDescriptor *sectionSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"publicationDate" ascending:NO];
    fetchRequest.sortDescriptors = @[sectionSortDescriptor, internalSortDescriptor];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"rssChannel == %@", self.rssChannel];
    aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:self.coreDataStack.managedObjectContext
                                                                      sectionNameKeyPath:@"dateForSection"
                                                                               cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}


@end
