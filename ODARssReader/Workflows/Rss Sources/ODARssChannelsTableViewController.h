//
//  ODARssChannelsTableViewController.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TyphoonAutoInjection.h"

@class ODARssChannelsViewModel, ODAApplicationAssembly;

@interface ODARssChannelsTableViewController : UITableViewController

@property (strong, nonatomic, nullable) InjectedClass(ODARssChannelsViewModel) viewModel;
@property (strong, nonatomic, nullable) InjectedClass(ODAApplicationAssembly) assembly;

@end
