//
//  ODARssChannelsViewModel.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ODACoreDataStack;
@class ODARssItemsTableViewModel, ODAApplicationAssembly;
@class RACSignal;

FOUNDATION_EXPORT NSString * _Nonnull const ODARssChannelViewModelErrorDomain;

typedef NS_ENUM(NSUInteger, ODARssChannelViewModelError) {
    ODARssChannelViewModelInvalidURLError
};

@interface ODARssChannelsViewModel : NSObject

@property (strong, nonatomic, nullable, readonly) NSString *title;
@property (strong, nonatomic, nonnull, readonly) RACSignal *contentUpdateSignal;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)initWithCoreDataStack: (id<ODACoreDataStack> _Nonnull) coreDataStack assembly:(ODAApplicationAssembly * _Nonnull) assembly
NS_DESIGNATED_INITIALIZER;

-(NSInteger)numberOfSections;
-(NSInteger)numberOfItemsInSection:(NSInteger)section;
-(NSString * _Nullable)titleForSection:(NSInteger)section;
-(NSString * _Nullable) cellTitleAtIndexPath:(NSIndexPath * _Nonnull)indexPath;
-(NSUInteger) cellRssItemsCountAtIndexPath:(NSIndexPath * _Nonnull)indexPath;

-(ODARssItemsTableViewModel * _Nonnull) viewModelForIndexPath: (NSIndexPath * _Nonnull) indexPath;

-(RACSignal * _Nonnull) addChannelNamed: (NSString * _Nonnull) name url: (NSString * _Nonnull) url;
-(RACSignal * _Nonnull) deleteChannelAtIndexPath: (NSIndexPath * _Nonnull) indexPath;
@end
