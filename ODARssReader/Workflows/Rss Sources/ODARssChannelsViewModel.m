//
//  ODARssChannelsViewModel.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODARssChannelsViewModel.h"
#import "Constants.h"
#import "ODAApplicationAssembly.h"
#import "ODACoreDataStack.h"
#import "ODARssCaching.h"
#import "RssChannel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

NSString * const ODARssChannelViewModelErrorDomain = @"ODARssChannelViewModelErrorDomain";

@interface ODARssChannelsViewModel() <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic, nonnull) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic, nonnull) id<ODACoreDataStack> coreDataStack;
@property (strong, nonatomic, nonnull) ODAApplicationAssembly *assembly;
@end

@implementation ODARssChannelsViewModel

- (instancetype _Nonnull)initWithCoreDataStack: (id<ODACoreDataStack> _Nonnull) coreDataStack
                                      assembly: (ODAApplicationAssembly * _Nonnull) assembly {
    self = [super init];
    if (self) {
        _coreDataStack = coreDataStack;
        _assembly = assembly;
        _title = NSLocalizedString(@"RSS Sources", @"");
        _contentUpdateSignal = [[[self rac_signalForSelector:@selector(controllerDidChangeContent:)
                                                fromProtocol:@protocol(NSFetchedResultsControllerDelegate)]
                                 map:^id(id value) {
                                     return nil;
                                 }] deliverOnMainThread];
    }
    return self;
}

-(ODARssItemsTableViewModel * _Nonnull) viewModelForIndexPath: (NSIndexPath * _Nonnull) indexPath {
    RssChannel *rssChannel = [self rssChannelAtIndexPath:indexPath];
    return [self.assembly rssItemsViewModelForRssChannel:rssChannel];
}

-(RACSignal * _Nonnull) addChannelNamed: (NSString * _Nonnull) name url: (NSString * _Nonnull) url {
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURL *sourceURL = [NSURL URLWithString:url];
        if (!(sourceURL && [sourceURL scheme] && [sourceURL host])) {
            [subscriber sendError:[self invalidURLError]];
            return nil;
        }
        NSString *sourceName = name;
        if (!sourceName.length) {
            sourceName = sourceURL.absoluteString;
        }
        [subscriber sendNext:RACTuplePack(sourceName, sourceURL)];
        
        [subscriber sendCompleted];
        return  nil;
    }] flattenMap:^RACStream *(RACTuple *args) {
        return [[self.assembly rssCacher] insertRssChannelNamed:args.first url:args.second];
    }] doCompleted:^{
        [self.coreDataStack saveMainContext];
    }];
}

-(NSError *) invalidURLError {
    return [NSError errorWithDomain:ODARssChannelViewModelErrorDomain code:ODARssChannelViewModelInvalidURLError
                                     userInfo:@{NSLocalizedDescriptionKey : NSLocalizedString(@"Invalid URL", @"")}];
}

-(RACSignal * _Nonnull) deleteChannelAtIndexPath: (NSIndexPath * _Nonnull) indexPath {
    RssChannel *rssChannel = [self rssChannelAtIndexPath:indexPath];
    return [[[self.assembly rssCacher] deleteRssChannel:rssChannel]
            doCompleted:^{
                [self.coreDataStack saveMainContext];
    }];
}


#pragma mark - Table View


-(NSInteger)numberOfSections {
    return [[self.fetchedResultsController sections] count];
}

-(NSInteger)numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return sectionInfo.numberOfObjects;
}

-(NSString * _Nullable)titleForSection:(NSInteger)section {
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return sectionInfo.name;
}

-(NSString * _Nullable) cellTitleAtIndexPath:(NSIndexPath * _Nonnull)indexPath {
    RssChannel *rssChannel = [self rssChannelAtIndexPath:indexPath];
    return rssChannel.name;
}

-(NSUInteger) cellRssItemsCountAtIndexPath:(NSIndexPath * _Nonnull)indexPath {
    RssChannel *rssChannel = [self rssChannelAtIndexPath:indexPath];
    return rssChannel.rssItems.count;
}

#pragma mark - Misc


-(RssChannel *) rssChannelAtIndexPath: (NSIndexPath *) indexPath {
    return  [self.fetchedResultsController objectAtIndexPath:indexPath];
}


#pragma mark - Fetched results controller


- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchedResultsController *aFetchedResultsController;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setFetchBatchSize:20];
    fetchRequest.entity = [NSEntityDescription entityForName: ODACoreDataRssChannelEntityName
                                      inManagedObjectContext:self.coreDataStack.managedObjectContext];
    NSSortDescriptor * internalSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    fetchRequest.sortDescriptors = @[internalSortDescriptor];
    aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:self.coreDataStack.managedObjectContext
                                                                      sectionNameKeyPath:nil
                                                                               cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

@end
