//
//  ODARssChannelsTableViewController.m
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODARssChannelsTableViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ODAApplicationAssembly.h"
#import "ODARssChannelTableViewCell.h"
#import "ODARssChannelsViewModel.h"
#import "ODARssItemsTableViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>


@interface ODARssChannelsTableViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addChannelBarButtonItem;
@end

@implementation ODARssChannelsTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self bindViewModel];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) bindViewModel {
    RAC(self, navigationItem.title) = RACObserve(self.viewModel, title);
    
    @weakify(self)
    [self.viewModel.contentUpdateSignal
     subscribeNext:^(RACTuple *argsFromBuffer) {
         @strongify(self);
         [self.tableView reloadData];
     }];
}


#pragma mark - Actions


- (IBAction)addChannelAction:(UIBarButtonItem *)sender {
    [self showAddChannelAlertView];
}


#pragma mark - Alerts

-(void) showAddChannelAlertView {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Add RSS source", @"")
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                              otherButtonTitles:NSLocalizedString(@"Add", @""), nil];
    
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [[alertView textFieldAtIndex:1] setSecureTextEntry:NO];
    [[alertView textFieldAtIndex:1] setPlaceholder:NSLocalizedString(@"Source URL", @"")];
    [[alertView textFieldAtIndex:0] setPlaceholder:NSLocalizedString(@"Souce Name", @"")];
    [alertView show];
}

-(void) showErrorAlertWithText: (NSString *) text {
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", @"")
                                                    message:text
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UIAlertViewDelegate


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [[[self.viewModel addChannelNamed:[alertView textFieldAtIndex:0].text url:[alertView textFieldAtIndex:1].text]
                 deliverOnMainThread]
                 subscribeError:^(NSError *error) {
                     [self showErrorAlertWithText:error.localizedDescription];
                 } completed:^{
         }];
    }
}


#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ODARssItemsTableViewController *itemsTableViewController = [self.assembly itemsTableViewControllerForViewModel:[self.viewModel viewModelForIndexPath:indexPath]];
    [self.navigationController pushViewController:itemsTableViewController animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
                                            forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[self.viewModel deleteChannelAtIndexPath:indexPath] subscribeCompleted:^{
        }];
    }
}


#pragma mark - UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.viewModel numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfItemsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ODARssChannelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(ODARssChannelTableViewCell.class)
                                                                       forIndexPath:indexPath];
    cell.rssChannelName.text = [self.viewModel cellTitleAtIndexPath:indexPath];
    [cell updateRssItemsCount:[self.viewModel cellRssItemsCountAtIndexPath:indexPath]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

@end
