//
//  ODARssChannelTableViewCell.h
//  ODARssReader
//
//  Created by Denis Obukhov on 12/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ODARssChannelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *rssChannelName;
- (void) updateRssItemsCount: (NSUInteger)count;
@end
