//
//  ODAApplicationAssembly.h
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "TyphoonAssembly.h"

@class ODARssChannelsViewModel, ODARssItemsTableViewModel, RssChannel, RssItem;
@class ODARssItemsTableViewController;
@class ODAWebViewModel, ODAWebViewController;

@protocol ODARssFeeding, ODARssCaching, ODACoreDataStack;

@interface ODAApplicationAssembly : TyphoonAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Services
//-------------------------------------------------------------------------------------------

- (id<ODACoreDataStack> _Nonnull) coreDataStack;
- (id<ODARssCaching> _Nonnull) rssCacher;
- (id<ODARssFeeding> _Nonnull) rssFeeder;

//-------------------------------------------------------------------------------------------
#pragma mark - View Controllers
//-------------------------------------------------------------------------------------------

-(ODARssItemsTableViewController * _Nonnull) itemsTableViewControllerForViewModel: (ODARssItemsTableViewModel * _Nonnull) viewModel;
-(ODAWebViewController * _Nullable) webViewControllerForViewModel: (ODAWebViewModel * _Nullable) viewModel;

//-------------------------------------------------------------------------------------------
#pragma mark - View Models
//-------------------------------------------------------------------------------------------

- (ODARssChannelsViewModel *_Nonnull) rssChannelsViewModel;
- (ODARssItemsTableViewModel *_Nonnull) rssItemsViewModelForRssChannel: (RssChannel * _Nonnull) rssChannel;
- (ODAWebViewModel *_Nonnull) webViewModelForRssItem: (RssItem * _Nonnull) rssItem;

@end
