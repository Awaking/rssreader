//
//  ODAApplicationAssembly.m
//  ODARssReader
//
//  Created by Denis Obukhov on 13/03/2018.
//  Copyright © 2018 denisobukhov. All rights reserved.
//

#import "ODAApplicationAssembly.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ODACoreDataStack.h"
#import "ODACoreDataStackImpl.h"
#import "ODACoreDataRssCacher.h"
#import "ODARssChannelsViewModel.h"
#import "ODANetworkXMLParserRssFeeder.h"
#import "ODARssItemsTableViewController.h"
#import "ODARssItemsTableViewModel.h"
#import "ODAWebViewController.h"
#import "ODAWebViewModel.h"
#import "RssChannel.h"

@implementation ODAApplicationAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Bootstrapping
//-------------------------------------------------------------------------------------------

- (AppDelegate *)appDelegate {
    return [TyphoonDefinition withClass:[AppDelegate class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(coreDataStack) with:[self coreDataStack]];
        [definition injectProperty:@selector(assembly) with:self];
    }];
}

- (UIStoryboard *) mainStoryboard {
    return [TyphoonDefinition withClass:[UIStoryboard class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(storyboardWithName:bundle:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:ODAMainStoryboardName];
            [initializer injectParameterWith:[NSBundle mainBundle]];
        }];
    }];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Services
//-------------------------------------------------------------------------------------------

- (id<ODACoreDataStack> _Nonnull) coreDataStack {
    return [TyphoonDefinition withClass:[ODACoreDataStackImpl class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeLazySingleton;
    }];
}

- (id<ODARssCaching> _Nonnull) rssCacher {
    return [TyphoonDefinition withClass:[ODACoreDataRssCacher class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithCoreDataStack:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self coreDataStack]];
        }];
    }];
}

- (id<ODARssFeeding> _Nonnull) rssFeeder {
    return [TyphoonDefinition withClass:[ODANetworkXMLParserRssFeeder class] configuration:^(TyphoonDefinition *definition) {
    }];
}

//-------------------------------------------------------------------------------------------
#pragma mark - View Controllers
//-------------------------------------------------------------------------------------------


-(ODARssItemsTableViewController *) itemsTableViewControllerForViewModel: (ODARssItemsTableViewModel*) viewModel {
    return [TyphoonDefinition withStoryboard:[self mainStoryboard]
                            viewControllerId:NSStringFromClass(ODARssItemsTableViewController.class)
                               configuration:^(TyphoonDefinition *definition) {
                                   [definition injectProperty:@selector(viewModel) with:viewModel];
                                   [definition injectProperty:@selector(assembly) with:self];
                               }];
}

-(ODAWebViewController * _Nullable) webViewControllerForViewModel: (ODAWebViewModel * _Nullable) viewModel {
    if (!viewModel)
        return nil;
    
    return [TyphoonDefinition withStoryboard:[self mainStoryboard]
                            viewControllerId:NSStringFromClass(ODAWebViewController.class)
                               configuration:^(TyphoonDefinition *definition) {
                                   [definition injectProperty:@selector(viewModel) with:viewModel];
                               }];
}

//-------------------------------------------------------------------------------------------
#pragma mark - View Models
//-------------------------------------------------------------------------------------------

- (ODARssChannelsViewModel *) rssChannelsViewModel {
    return [TyphoonDefinition withClass:[ODARssChannelsViewModel class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithCoreDataStack:assembly:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self coreDataStack]];
            [initializer injectParameterWith:self];
        }];
    }];
}

- (ODARssItemsTableViewModel *) rssItemsViewModelForRssChannel: (RssChannel *) rssChannel {
    return [TyphoonDefinition withClass:[ODARssItemsTableViewModel class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithCoreDataStack:rssChannel:assembly:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self coreDataStack]];
            [initializer injectParameterWith:rssChannel];
            [initializer injectParameterWith:self];
        }];
    }];
}

- (ODAWebViewModel *) webViewModelForRssItem: (RssItem *) rssItem {
    return [TyphoonDefinition withClass:[ODAWebViewModel class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithRssItem:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:rssItem];
        }];
    }];
}

@end
